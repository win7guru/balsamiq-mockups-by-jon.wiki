# Techniques
1. Masking objects
2. Shading
3. Alignment
4. Integration with Other Programs

# Masking

Masking, to me is the process of creating multiple layers in a mockup and using the least amount of controls
Another explanation, perhaps a more accurate one can be found on the Balsamiq UX Blog [Creating a Masking effect using Balsamiq](http://blogs.balsamiq.com/ux/2011/09/20/creating-a-mask-effect/)

# Shading

Shading, to me is the process of creating a gradient that gives the control a more polished look.  This can be done using multiple colors or carefully ordered masks.

# Alignment

Alignment, to me is an integral part to creating a nice mockup.  Aligning an object has many benefits, such as:
       1. Precise layout of controls
       2. Better look and feel for end users 

# Integration with Other Programs

Integration with Other Programs can be as simple as copying and pasting from [Creating a pivot table in Balsamiq using Excel](http://blogs.balsamiq.com/ux/2011/03/11/using-excel-data-to-create-a-pivot-table/) or Word.  But it is not limited to word processors, it can also include such programs as [Jitbit Macro Recorder website](http://www.jitbit.com/macro-recorder/) to provide automation.  Another integration of Balsamiq, that I have discovered, is that of [Definition of Expression Design](http://en.wikipedia.org/wiki/Microsoft_Expression_Design).  With it, there is a process of importing an image (ie, a rectangle control, a datagrid, etc) into the program and converting that to [Definition of XAML](http://en.wikipedia.org/wiki/XAML)
